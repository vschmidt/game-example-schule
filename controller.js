function Game(bugs, flower, board) {
	this.bugs = bugs;
	this.flower = flower;
	this.board = board;
	
	this.timeTick = function () {
		for (var i = 0; i < this.bugs.length; i++) {

			if (flower.isInside(this.bugs[i])) {
				return i;
			}

			this.bugs[i].forward();
			if (!this.board.isInside(this.bugs[i])) this.bugs[i].backward();
		}
		
		return -1;		
	}
}
