function Bug(x, y, radius) {
	this.x = x;
	this.y = y;
	this.radius = radius;
	this.direction = 0;
	this.speed = 0;
	
	this.changeSpeed = function (step) {
		this.speed += step;
		this.speed = Math.min(Math.max(this.speed, this.minSpeed), this.maxSpeed);
	}
	this.turnLeft = function () {
		this.direction -= 10;
	}
	this.turnRight = function () {
		this.direction += 10;
	}
	this.forward = function () {
		this.x += this.speed * Math.sin(this.direction / 180 * Math.PI);
		this.y -= this.speed * Math.cos(this.direction / 180 * Math.PI);
	}
	this.backward = function () {
		this.x -= this.speed * Math.sin(this.direction / 180 * Math.PI);
		this.y += this.speed * Math.cos(this.direction / 180 * Math.PI);
	}
}

Bug.prototype.minSpeed = -10;
Bug.prototype.maxSpeed = 10;

function Flower(x, y, radius) {
	this.x = x;
	this.y = y;
	this.radius = radius;
	
	this.isInside = function (bug) {
		var dx = bug.x - this.x;
		var dy = bug.y - this.y;
		
		return Math.sqrt(dx * dx + dy * dy) < this.radius + bug.radius;
	}
}

function Board(width, height) {
	this.width = width;
	this.height = height;
	
	this.isInside = function (bug) {
		return bug.x >= 0 & bug.y >= 0 & bug.x < this.width & bug.y < this.height;
	}
}
